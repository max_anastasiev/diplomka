package com.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SubmitNewsAsFreelancer {

    private static String url = "http://gin.cs1.soft-tehnica.com/news/freelance/add";

    public SubmitNewsAsFreelancer(WebDriver driver){
        driver.get(url);
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//*[@id=\"edit-field-new-media-type-und\"]/div[1]/label/i")
    private WebElement mediatype;

    @FindBy(xpath = "//*[@id=\"edit-field-slug-text-und-0-value\"]")
    private WebElement slug;

    @FindBy(xpath = "//*[@id=\"edit-title\"]")
    private WebElement headline;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-text-und-0-summary\"]")
    private WebElement summary;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-text-und-0-value\"]")
    private WebElement story;



    //Notes bar
    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[1]/a")
    private WebElement legalnotesbutton;

    @FindBy(xpath = "//*[@id=\"edit-field-legal-notes-news-und-0-value\"]")
    private WebElement legalnotes;

    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[2]/a")
    private WebElement backgroundinfobutton;

    @FindBy(xpath = "//*[@id=\"edit-field-bg-info-notes-news-und-0-value\"]")
    private WebElement backgroundinfo;

    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[3]/a")
    private WebElement alreadyoutbutton;

    @FindBy(xpath = "//*[@id=\"edit-field-already-out-notes-news-und-0-value\"]")
    private WebElement alreadyout;

    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[4]/a")
    private WebElement archivebutton;

    @FindBy(xpath = "//*[@id=\"edit-field-archive-notes-news-und-0-value\"]")
    private WebElement archive;

    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[5]/a")
    private WebElement suggestsendingbutton;

    @FindBy(xpath = "//*[@id=\"edit-field-suggest-sending-news-und-0-value\"]")
    private WebElement suggestsending;



    //Original source
    @FindBy(name = "files[field_inline_text_source_und_form_field_source_material_und_0_field_sm_file_und_0]")
    private WebElement sourcechoosefile;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-source-material-und-0-field-sm-file-und-0-upload-button\"]")
    private WebElement sourceuploadfile;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-source-material-und-0-remove-button--3\"]")
    private WebElement deleteuploadedfile;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-source-material-und-add-more--5\"]")
    private WebElement addanotheritem;


    //Add picture
    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-news-link-und-0-url\"]")
    private WebElement picturesource;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-news-copyright-und-0-value\"]")
    private WebElement picturecopyright;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-picture-description-und-0-value\"]")
    private WebElement picturecaption;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-img-news-original-und-0-upload\"]")
    private WebElement picturechoosefile;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-img-news-original-und-0-upload-button\"]")
    private WebElement pictureupload;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-actions-ief-add-save\"]")
    private WebElement addpicture;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-actions-ief-add-cancel\"]")
    private WebElement cancelpicture;


    //Keywords
    @FindBy(xpath = "//*[@id=\"edit-field-news-geography-und-0-field-geography-url-und-0-url\"]")
    private WebElement geography;

    @FindBy(xpath = "//*[@id=\"news-form-all-bundle\"]/div/div/div[11]/div/div[2]/ul/li[2]/a")
    private WebElement subjecttab;

    @FindBy(xpath = "//*[@id=\"edit-field-keywords-und\"]/ul/li[2]/ul/li[2]/div[2]/label")
    private WebElement subject;

    @FindBy(xpath = "//*[@id=\"news-form-all-bundle\"]/div/div/div[11]/div/div[2]/ul/li[3]/a")
    private WebElement typemedia;

    @FindBy(xpath = "//*[@id=\"edit-field-media-type-tags-und-0-679-679-children-684-684-children-688-688\"]")
    private WebElement typemediacheck;


    //Spotter Information
    @FindBy(xpath = "//*[@id=\"edit-field-spotter-urgency-und\"]")
    private WebElement spotterurgency;

    @FindBy(xpath = "//*[@id=\"edit-field-spotter-text-rating-und\"]")
    private WebElement storyrating;

    @FindBy(xpath = "//*[@id=\"edit-field-spotter-media-rating-und\"]")
    private WebElement picturerating;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-name-und-0-value\"]")
    private  WebElement name;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-surname-und-0-value\"]")
    private WebElement surname;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-email-und-0-value\"]")
    private WebElement email;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-phone-und-0-value\"]")
    private WebElement phone;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-country-und\"]")
    private WebElement spotterlocation;



    //Buttons
    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-country-und\"]")
    private WebElement cancel;

    @FindBy(xpath = "//*[@id=\"news-form-all-bundle\"]/div/div/div[20]/div/div/button[2]")
    private WebElement storycontacts;

    @FindBy(xpath = "//*[@id=\"edit-submit-edit\"]")
    private WebElement saveandcontinue;





    public WebElement getAlreadyout() {
        return alreadyout;
    }

    public WebElement getAlreadyoutbutton() {
        return alreadyoutbutton;
    }

    public WebElement getArchive() {
        return archive;
    }

    public WebElement getArchivebutton() {
        return archivebutton;
    }

    public WebElement getBackgroundinfo() {
        return backgroundinfo;
    }

    public WebElement getBackgroundinfobutton() {
        return backgroundinfobutton;
    }

    public WebElement getHeadline() {
        return headline;
    }

    public WebElement getLegalnotes() {
        return legalnotes;
    }

    public WebElement getLegalnotesbutton() {
        return legalnotesbutton;
    }

    public WebElement getSlug() {
        return slug;
    }

    public WebElement getStory() {
        return story;
    }

    public WebElement getSuggestsending() {
        return suggestsending;
    }

    public WebElement getSuggestsendingbutton() {
        return suggestsendingbutton;
    }

    public WebElement getSummary() {
        return summary;
    }

    public WebElement getMediatype() {
        return mediatype;
    }

    public WebElement getChoosefile() {
        return sourcechoosefile;
    }

    public WebElement getSourceuploadfile() {
        return sourceuploadfile;
    }

    public WebElement getAddanotheritem() {
        return addanotheritem;
    }

    public WebElement getAddpicture() {
        return addpicture;
    }

    public WebElement getCancel() {
        return cancel;
    }

    public WebElement getCancelpicture() {
        return cancelpicture;
    }

    public WebElement getDeleteuploadedfile() {
        return deleteuploadedfile;
    }

    public WebElement getEmail() {
        return email;
    }

    public WebElement getTypemedia() {
        return typemedia;
    }

    public WebElement getSurname() {
        return surname;
    }

    public WebElement getStoryrating() {
        return storyrating;
    }

    public WebElement getSaveandcontinue() {
        return saveandcontinue;
    }

    public WebElement getPicturecaption() {
        return picturecaption;
    }

    public WebElement getSpotterlocation() {
        return spotterlocation;
    }

    public WebElement getPicturerating() {
        return picturerating;
    }

    public WebElement getSubject() {
        return subject;
    }

    public WebElement getSubjecttab() {
        return subjecttab;
    }

    public WebElement getPicturechoosefile() {
        return picturechoosefile;
    }

    public WebElement getSourcechoosefile() {
        return sourcechoosefile;
    }

    public WebElement getSpotterurgency() {
        return spotterurgency;
    }

    public WebElement getStorycontacts() {
        return storycontacts;
    }

    public WebElement getName() {
        return name;
    }

    public WebElement getPhone() {
        return phone;
    }

    public WebElement getGeography() {
        return geography;
    }

    public WebElement getPicturecopyright() {
        return picturecopyright;
    }

    public WebElement getPicturesource() {
        return picturesource;
    }

    public WebElement getPictureupload() {
        return pictureupload;
    }

    public WebElement getTypemediacheck() {
        return typemediacheck;
    }
}
