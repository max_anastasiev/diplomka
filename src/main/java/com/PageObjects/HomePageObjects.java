package com.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePageObjects {

    private static String url = "http://gin.cs1.soft-tehnica.com/";

    public HomePageObjects(WebDriver driver) {
        driver.get(url);
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//*[@id=\"terms\"]")
    private WebElement termscheckbox;

    @FindBy(xpath = "//*[@id=\"block-block-1\"]/p[2]/a")
    private WebElement submitnews;

    @FindBy(xpath = "//*[@id=\"block-block-1\"]/div/div[2]/a")
    private WebElement loginbutton;

    @FindBy(xpath = "//*[@id=\"block-block-1\"]/div/div[1]/a")
    private  WebElement contactbutton;

    public WebElement getContactbutton() {
        return contactbutton;
    }

    public WebElement getSubmitnews() {
        return submitnews;
    }

    public WebElement getTermscheckbox() {
        return termscheckbox;
    }

    public WebElement getLoginbutton() {
        return loginbutton;
    }

    public static void setUrl(String url) {
        HomePageObjects.url = url;
    }
}
