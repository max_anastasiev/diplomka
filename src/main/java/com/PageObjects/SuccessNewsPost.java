package com.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SuccessNewsPost {

    @FindBy(xpath = "//*[@id=\"block-system-main\"]/div/div/div/div[1]")
    private WebElement successmessage;

    public SuccessNewsPost(WebDriver driver) {
    }

    public WebElement getSuccessmessage() {
        return successmessage;
    }
}
