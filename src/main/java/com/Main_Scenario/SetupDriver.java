package com.Main_Scenario;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Parameter;
import ru.yandex.qatools.allure.annotations.Step;

public class SetupDriver {

public WebDriver driver = new ChromeDriver();
public WebDriverWait wait;

    public String url = "http://gin.cs1.soft-tehnica.com";
//    public String login = "god";
//    public String pass = "but_not_real";

    @Parameter("OS")
    private String os;
    @Parameter("Browser")
    private String browser;

//    @Attachment(value = "Page screenshot", type = "image/png")
//    public byte[] saveScreenshot(byte[] screenShot) {
//        return screenShot;
//    }

//    @Rule
//    public ScreenshotTestRule screenshotTestRule = new ScreenshotTestRule(driver);

    @Before
    public void setUp() {
        os = System.getProperty("os.name");
        browser = "Chrome_" + driver;
        Initialize(url);
//        Login(login, pass);
    }

    @Step("Open browser and verifying title")
    public void Initialize(String url) {
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 10, 500);
        driver.get(url);
        Assert.assertEquals(driver.getTitle(), "Welcome to demo.t4hub | demo.t4hub");
    }

//    @Step("Accesez sistemul cu = Login-{0},Password-{1}")
//    public void Login(String login, String pass) {
//        driver.findElement(By.id("edit-name")).sendKeys(login);
//        driver.findElement(By.id("edit-pass")).sendKeys(pass);
//        driver.findElement(By.id("edit-submit")).click();
//        Assert.assertEquals(driver.getTitle(), login + " | demo.t4hub");
//    }

    @After
    @Step("Inchid browser")
    public void close() {
        driver.close();
        driver.quit();
    }
}
