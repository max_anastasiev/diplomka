package com.Main_Scenario;

import static org.hamcrest.CoreMatchers.is;

import com.Data.GenerateData;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;


public class MS_TC_001 extends Class_Initialization {


    @Test
    public void postNewsAsFreelancer() throws InterruptedException{

        //init GenerateData class
        GenerateData generateData = new GenerateData();


        //Check terms check-box, verify and press submit news button
        homePageObjects.getTermscheckbox().click();
        String submitnews = homePageObjects.getSubmitnews().getText().toUpperCase();
        Assert.assertThat(submitnews, is("SUBMIT A NEWS ITEM FOR CONSIDERATION"));
        homePageObjects.getSubmitnews().click();

        //Submit news
        submitNewsAsFreelancer.getMediatype().click();

        submitNewsAsFreelancer.getSlug().sendKeys(generateData.generateRandomString(10));
        submitNewsAsFreelancer.getHeadline().sendKeys(generateData.generateRandomAlphaNumeric(20));
        submitNewsAsFreelancer.getSummary().sendKeys(generateData.generateRandomString(60));
        submitNewsAsFreelancer.getStory().sendKeys(generateData.generateRandomString(120));
        submitNewsAsFreelancer.getLegalnotes().sendKeys(generateData.generateRandomString(10));
        submitNewsAsFreelancer.getBackgroundinfobutton().click();

        //Please change file for upload before start test
        submitNewsAsFreelancer.getChoosefile().sendKeys("C:\\Users\\Max\\Documents\\200.jpg");
        submitNewsAsFreelancer.getSourceuploadfile().sendKeys(Keys.SPACE);

        //Keywords
        submitNewsAsFreelancer.getGeography().sendKeys(generateData.generateRandomString(7));
        submitNewsAsFreelancer.getSubjecttab().click();

        WebElement subject = wait.until(ExpectedConditions.elementToBeClickable(submitNewsAsFreelancer.getSubject()));
        subject.click();

        submitNewsAsFreelancer.getTypemedia().click();
        submitNewsAsFreelancer.getTypemediacheck().click();

        //Spotter info
        WebElement dropdown = submitNewsAsFreelancer.getSpotterurgency();
        submitNewsAsFreelancer.getSpotterurgency().click();
        Select dropdownurgency = new Select(dropdown);
        dropdownurgency.selectByValue("2");
        WebElement dropdown1 = submitNewsAsFreelancer.getStoryrating();
        submitNewsAsFreelancer.getStoryrating().click();
        Select dropdownrating = new Select(dropdown1);
        dropdownrating.selectByVisibleText(generateData.generateRandomNumber(1));
        MS_TC_001.captureScreenShot(driver);






//
//        //init SuccesNewsPost class
//        SuccessNewsPost successNewsPost = new SuccessNewsPost();
//
//        //check for success message
//        Assert.assertThat(successNewsPost.getSuccessmessage().getText(), is("Your Account Has Been Created!"));



    }


    public static void captureScreenShot(WebDriver ldriver){
        // Take screenshot and store as a file format
        File src=((TakesScreenshot)ldriver).getScreenshotAs(OutputType.FILE);
        try {
            // now copy the  screenshot to desired location using copyFile method
            FileUtils.copyFile(src, new File("C:/Users/Max/Desktop/Allied Testing/diplomka/target/generated-sources/screenshot/"+System.currentTimeMillis()+".png"));
            }
            catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

}
